<?php
namespace Two\Utils\UrlReader;

class Wordpress implements ReaderInterface {

    public function __construct() {
        if (!function_exists('wp_remote_get')) {
            throw new \RuntimeException("Function 'wp_remote_get' has to be defined.");
        }
        if (!function_exists('is_wp_error')) {
            throw new \RuntimeException("Function 'is_wp_error' has to be defined.");
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see \Two\Utils\UrlReader\ReaderInterface::read()
     */
    public function read($url) {
        $return = wp_remote_get($url); //call WP function
        if (is_wp_error($return))  {
            throw new \UnexpectedValueException($return->get_error_message());
        }
        if (empty($return['body'])) {
            throw new \UnexpectedValueException("Response body can't be empty.");
        }
        return $return['body'];
    }

}