<?php
namespace Two\Utils\UrlReader;

class PhpWrapper implements ReaderInterface {

    /**
     *
     * {@inheritDoc}
     * @see \Two\Utils\UrlReader\ReaderInterface::read()
     */
    public function read($url) {
        $output = @file_get_contents($url);
        if (empty($output)) {
            throw new \UnexpectedValueException("Response body can't be empty.");
        }
        return $output;
    }

}